(function(window, document, undefined) {

    var Banner = (function() {
		
		var mini,
			is_firefox = navigator.userAgent.indexOf('Firefox') > -1,
			is_IE = navigator.userAgent.indexOf('MSIE') !== -1,
			is_IE11 = navigator.appVersion.indexOf('Trident/') > 0;
		
        // Initialize Miniscroll.js for scrolling // Please use http://miniscroll.rogerluizm.com.br/ for documentation
        mini = new Miniscroll("#scroller", {
            axis: "y", // defines vertical scrolling
            size: 13, // defines width of scrollbar
            sizethumb: "auto", // defines width/height of thumb
            scrollbarSize: 261, // define height of scrollbar
            thumbColor: "#7f9bab",
            trackerColor: "#d1d2d4",
            mousewheel: true // enables mousewheel scrolling
        });

        mini.scrollTo(0);

        function scrollComplete() {
            var seed;
            seed = setInterval(function() {
                mini.scrollTo(0);
                clearTimeout(seed);
            }, 800);
        }

        var seed2;

        seed2 = setInterval(function() {
            //mini.autoScrollStart(.14, scrollComplete);
			if (navigator.userAgent.indexOf('Mac OS X') != -1) {
				console.log("MAC");
				if(is_firefox) mini.autoScrollStart(0.07, scrollComplete); //.014 15s - 0.11 20s
				else if(is_IE || is_IE11) mini.autoScrollStart(0.06, scrollComplete); //0.11
				else mini.autoScrollStart(0.052, scrollComplete); //0.10 15s - 0.08 20s
			} else {
				console.log("PC and Other");
				if(is_firefox) mini.autoScrollStart(0.098, scrollComplete); //0.20 15s - 0.155 20s
				else if(is_IE || is_IE11) mini.autoScrollStart(0.06, scrollComplete); //0.11 15s - 0.1 20s
				else mini.autoScrollStart(0.059, scrollComplete); //0.09 15s - 0.09 20s
			}
            clearTimeout(seed2);
        }, 500);

        // ************************************************************
        // If Enabler loaded, the init will be fired
        // ************************************************************
        function init() {

            var boundEvents = {};

            function bind(elem, eventName, callback) {
                if (elem.addEventListener) {
                    elem.addEventListener(eventName, callback, false);
                } else if (elem.attachEvent) {
                    var eID = elem.attachEvent('on' + eventName, callback);
                    boundEvents[eID] = { name: eventName, callback: callback };
                }
            }

            function unbind(elem, eventName, callback) {
                if (elem.removeEventListener) {
                    elem.removeEventListener(eventName, callback, false);
                } else if (elem.detachEvent) {
                    for (var eID in boundEvents) {
                        if ((boundEvents[eID].name === eventName) &&
                            (boundEvents[eID].callback === callback)) {
                            elem.detachEvent(eID);
                            delete boundEvents[eID];
                        }
                    }
                }
            }

            // CTA BTN CLICKTAG //
            bind(cta, 'click', function(e) {
                e.preventDefault();
                Enabler.exit("clickTag1");
            });

            bind(pi, 'click', function(e) {
                e.preventDefault();
                Enabler.exit("clickTag2");
            });

            bind(logo, 'click', function(e) {
                e.preventDefault();
                Enabler.exit("clickTag3");
            });
        }

        return {
            initialize: function() {
                init();
            }
        };

    // Public API
    }());

    window.Banner = Banner;

}(window, document));


// ************************************************************
// *        Enabler.js - Doubleclick API                      *
// ************************************************************ 

function politeLoad(urls, onComplete) {
    var l = urls.length,
        loaded = 0,
        checkProgress = function() {
            if (++loaded === l && onComplete) {
                onComplete();
            } else {}
        },
        i;

    for (i = 0; i < l; i++) {
        Enabler.loadScript(urls[i], checkProgress);
    }
}

// If true, start function "building". If false, listen for INIT.
if (Enabler.isInitialized()) {
    enablerInitHandler();
} else {
    Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
}

function enablerInitHandler() {
    if (Enabler.isPageLoaded()) {
        pageLoadedHandler();

    } else {
        Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, pageLoadedHandler);
    }
}

function pageLoadedHandler() {
    Banner.initialize();
}